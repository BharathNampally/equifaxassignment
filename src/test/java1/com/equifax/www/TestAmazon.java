package com.equifax.www;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.equifax.www.pages.CheckoutPage;
import com.equifax.www.pages.HomePage;
import com.equifax.www.pages.ProductPage;
import com.equifax.www.pages.SearchPage;

public class TestAmazon {
	@Test
	public void amazonTest() throws IOException, InterruptedException {
		BaseTest baseTest=new BaseTest();
		WebDriver driver=baseTest.openChromeBrowser();
		ReadProperty readProperties=new ReadProperty();
		String url=readProperties.getProperty("applicationURL");
		driver.get(url);
		HomePage homePage=new HomePage();
		homePage.selectCategoryType(driver, readProperties.getProperty("categoryName"));
		homePage.searchProductName(driver, readProperties.getProperty("productName"));
		SearchPage searchPage=new SearchPage();
		String productPrice=searchPage.selectFirstProduct(driver);
		ProductPage  productPage=new ProductPage();
		productPage.validateProductPriceAndAddtoCart(driver, productPrice);
		CheckoutPage checkOutPage=new CheckoutPage();
		checkOutPage.validateCheckOutAmountAndProceedToCheckOut(driver, productPrice);
	}

}
