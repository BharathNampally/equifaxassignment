package com.equifax.www.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class CheckoutPage {
	public By getcheckOutPrice() {
		return By.xpath("//div[@id='hlb-subcart']/div/span/span[2]");
	}
	public By getProceedToCheckoutButton() {
		return By.id("hlb-ptc-btn");
	}
	public void validateCheckOutAmountAndProceedToCheckOut(WebDriver driver,String expectedPrice) {
		Assert.assertEquals(driver.findElement(getcheckOutPrice()).getText(), expectedPrice,"Incorrect checkout price");
		driver.findElement(getProceedToCheckoutButton()).click();
	}

}
