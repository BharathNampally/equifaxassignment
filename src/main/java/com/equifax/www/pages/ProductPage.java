package com.equifax.www.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class ProductPage {
	public By getAddToCart() {
		return By.id("add-to-cart-button");
	}
	public By getProductPrice() {
		return By.id("newBuyBoxPrice");
	}
	public void validateProductPriceAndAddtoCart(WebDriver driver, String expectedPrice) {
		Assert.assertEquals(driver.findElement(getProductPrice()).getText(),expectedPrice,"Incorrect product price");
		driver.findElement(getAddToCart()).click();
	}
}
