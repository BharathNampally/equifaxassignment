package com.equifax.www.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class HomePage {
	public By getCategoryDD() {
		return By.name("url");
	}
	public By getProductName() {
		return By.id("twotabsearchtextbox");
	}
	public By getsearchButton() {
		return By.id("nav-search-submit-button");
	}
	public void selectCategoryType(WebDriver driver,String categoryName) {
		Select dd=new Select(driver.findElement(getCategoryDD()));
		dd.selectByVisibleText(categoryName);
	}
	public void searchProductName(WebDriver driver,String productName) {
		driver.findElement(getProductName()).sendKeys(productName);
		driver.findElement(getsearchButton()).click();
		
	}
}
