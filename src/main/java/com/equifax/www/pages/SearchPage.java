package com.equifax.www.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage {
	
	public By getFirstProduct() {
		return By.xpath("//div[@data-cel-widget='search_result_1']");
	}
	public By productLink() {
		return By.xpath(".//h2/a");
	}
	public By productPrice() {
		return By.className("a-price");
	}
	public String selectFirstProduct(WebDriver driver) {
		WebDriverWait wait=new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(getFirstProduct()));
		String productPrice=driver.findElement(getFirstProduct()).findElement(productPrice()).getText();
		productPrice=productPrice.replaceAll("\n", ".");
		driver.findElement(getFirstProduct()).findElement(productLink()).click();
		return productPrice;
	}

}
