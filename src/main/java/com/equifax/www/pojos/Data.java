package com.equifax.www.pojos;

import lombok.Getter;
import lombok.Setter;
@Getter 
public class Data {
	
	private String id;
	private String employee_name;
	private String employee_salary;
	private String employee_age;
	private String profile_image;
}
